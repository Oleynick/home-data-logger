from django.contrib import admin
from .models import Sensor, DataPoint


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = ('name', 'location')


@admin.register(DataPoint)
class DataPointAdmin(admin.ModelAdmin):
    list_display = ('sensor', 'value', 'timestamp')
    list_filter = ('sensor', 'timestamp')
