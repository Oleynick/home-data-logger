from django.db import models

from django.db import models


class Sensor(models.Model):
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    unit = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class DataPoint(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.sensor.name} - {self.timestamp}"

