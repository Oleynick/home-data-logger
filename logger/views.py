from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Sensor, DataPoint


def sensor_readings(request):
    sensors = Sensor.objects.all()

    for sensor in sensors:
        sensor.latest_reading = sensor.datapoint_set.latest('timestamp')

    return render(request, 'sensor_readings.html', {'sensors': sensors})


@csrf_exempt
def add_sensor_data(request):
    if request.method == 'POST':
        sensor_name = request.POST.get('sensor_name')
        value = request.POST.get('value')

        print(sensor_name, value)

        if sensor_name and value:
            try:
                sensor = Sensor.objects.get(name=sensor_name)
                data_point = DataPoint.objects.create(sensor=sensor, value=value)
                print(data_point)
                return JsonResponse({'success': True, 'message': 'Data point added successfully'})
            except Sensor.DoesNotExist:
                return JsonResponse({'success': False, 'message': 'Sensor not found'}, status=404)
        else:
            return JsonResponse({'success': False, 'message': 'Sensor name and value are required'}, status=400)
    else:
        return JsonResponse({'success': False, 'message': 'Only POST requests are allowed'}, status=405)
